import time
import pytest
from base64 import b64encode
from lorem import sentence as lorem_sentence, paragraph as lorem_paragraph
import requests

blog_url = 'https://gaworski.net'
posts_endpoints_url = f'{blog_url}/wp-json/wp/v2/posts'
comments_endpoints_url = f'{blog_url}/wp-json/wp/v2/comments'

username_editor = 'editor'
password_editor = 'HWZg hZIP jEfK XCDE V9WM PQ3t'

username_commenter = 'commenter'
password_commenter = 'SXlx hpon SR7k issV W2in zdTb'

def get_token(username, password):
    return b64encode(f"{username}:{password}".encode('utf-8')).decode('ascii')

token_editor = get_token(username_editor, password_editor)
token_commenter = get_token(username_commenter, password_commenter)

@pytest.fixture(scope='module')
def article():
    timestamp = int(time.time())
    article = {
        "article_creation_date": timestamp,
        "article_title": "This is new post " + str(timestamp),
        "article_subtitle": lorem_sentence(),
        "article_text": lorem_paragraph()
    }
    return article

@pytest.fixture(scope='module')
def headers_editor():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_editor
    }
    return headers

@pytest.fixture(scope='module')
def headers_commenter():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_commenter
    }
    return headers

@pytest.fixture(scope='module')
def posted_article(article, headers_editor):
    payload = {
        "title": article["article_title"],
        "excerpt": article["article_subtitle"],
        "content": article["article_text"],
        "status": "publish",
        "author": 1
    }
    response = requests.post(url=posts_endpoints_url, headers=headers_editor, json=payload)
    response.raise_for_status()
    return response

def test_new_post_successfully_created(posted_article):
    assert posted_article.status_code == 201
    response_data = posted_article.json()
    assert response_data['author'] == 1

def test_newly_created_post_can_be_read(article, posted_article):
    wordpress_post_id = posted_article.json()["id"]
    wordpress_post_url = f'{posts_endpoints_url}/{wordpress_post_id}'
    published_article = requests.get(url=wordpress_post_url)
    assert published_article.status_code == 200
    wordpress_post_data = published_article.json()
    assert wordpress_post_data["title"]["rendered"] == article["article_title"]
    assert wordpress_post_data["excerpt"]["rendered"] == f'<p>{article["article_subtitle"]}</p>\n'
    assert wordpress_post_data["status"] == 'publish'

def test_post_can_be_updated(article, posted_article, headers_editor):
    wordpress_post_id = posted_article.json()["id"]
    wordpress_post_url = f'{posts_endpoints_url}/{wordpress_post_id}'
    payload = {
        "excerpt": "This is new excerpt"
    }
    updated_article = requests.post(url=wordpress_post_url, json=payload, headers=headers_editor)
    assert updated_article.status_code == 200
    published_article = requests.get(url=wordpress_post_url)
    wordpress_post_data = published_article.json()
    assert wordpress_post_data["excerpt"]["rendered"] == f'<p>{payload["excerpt"]}</p>\n'
    assert wordpress_post_data["title"]["rendered"] == article["article_title"]
    assert wordpress_post_data["status"] == 'publish'

@pytest.fixture(scope='module')
def posted_comment(posted_article, headers_commenter):
    wordpress_post_id = posted_article.json()["id"]
    comment_data = {
        "post": wordpress_post_id,
        "content": "To jest komentarz",
        "author_name": "Commenter",
        "author_email": "commenter@example.com"
    }
    response = requests.post(url=comments_endpoints_url, headers=headers_commenter, json=comment_data)
    response.raise_for_status()
    return response

def test_comment_successfully_created(posted_comment, posted_article):
    assert posted_comment.status_code == 201
    comment_data = posted_comment.json()
    assert comment_data['post'] == posted_article.json()["id"]
    assert comment_data['author_name'] == 'Commenter'

@pytest.fixture(scope='module')
def posted_reply(posted_comment, headers_editor):
    comment_id = posted_comment.json()["id"]
    post_id = posted_comment.json()["post"]
    reply_data = {
        "post": post_id,
        "content": "To jest odpowiedź na komentarz",
        "parent": comment_id,
        "author_name": "Editor",
        "author_email": "editor@example.com"
    }
    response = requests.post(url=comments_endpoints_url, headers=headers_editor, json=reply_data)
    response.raise_for_status()
    return response

def test_reply_successfully_created(posted_reply, posted_comment, posted_article):
    assert posted_reply.status_code == 201
    reply_data = posted_reply.json()
    assert reply_data['parent'] == posted_comment.json()["id"]
    assert reply_data['post'] == posted_article.json()["id"]
    assert reply_data['author_name'] == 'Editor'